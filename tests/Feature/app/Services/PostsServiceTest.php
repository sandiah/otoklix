<?php

namespace Tests\Feature\app\Services;

use App\Models\Posts;
use App\Services\PostsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutEvents;
use Mockery;
use Tests\TestCase;

class PostsServiceTest extends TestCase
{
    use WithoutEvents;
    use DatabaseTransactions;

    private $service;

    protected function setUp() : void
    {
        parent::setUp();

        $this->service = new PostsService();
    }

    public function testAdd_ShouldSuccess()
    {
        $params = [
            'title' => 'Maudy Ayunda',
            'content' => 'Maudy Ayunda added a new post',
            'published_at' => '2021-10-13T05:22:12.052Z'
        ];

        $result = $this->service->add($params);
        $this->assertInstanceOf('App\Models\Posts', $result);
    }

    public function testUpdate_ShouldSuccess()
    {
        $posts = Posts::factory()->create([
            'title' => 'Muady Ayunda',
            'content' => 'Maudy Ayunda added a new posts'
        ]);

        $result = $this->service->update($posts->id, [
            'content' => 'Maudy Ayunda updated her post',
            'published_at' => '2021-10-13T05:22:12.052Z'
        ]);

        $this->assertInstanceOf('App\Models\Posts', $result);
        $this->assertEquals('Maudy Ayunda updated her post', $result->content);
    }

    public function testUpdate_ShouldFailed()
    {
        Posts::factory()->create([
            'title' => 'Muady Ayunda',
            'content' => 'Maudy Ayunda added a new posts'
        ]);

        $result = $this->service->update(100, [
            'content' => 'Maudy Ayunda updated her post',
            'published_at' => '2021-10-13T05:22:12.052Z'
        ]);

        $this->assertNull($result);
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }

}

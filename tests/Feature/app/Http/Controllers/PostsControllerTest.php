<?php

namespace Tests\Feature\app\Http\Controllers;

use App\Models\Posts;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutEvents;
use Mockery;
use Tests\TestCase;

class PostsControllerTest extends TestCase
{
    use WithoutEvents;
    use DatabaseTransactions;

    const URL = 'api/posts';

    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testIndex_ShouldSuccess()
    {
        $this->mockPosts();

        $result = $this->json('GET', self::URL);
        $response = $result->getOriginalContent();

        $this->assertEquals('success', $response['status']);
        $this->assertEquals(200, $response['code']);
        $this->assertNotEmpty($response['data']);
    }

    public function testStore_ShouldSuccess()
    {
        $result = $this->json('POST', self::URL, [
            'title' => 'Unit Test',
            'content' => 'Testing feature',
            'published_at' => '2021-10-13T05:22:12.052Z'
        ]);

        $response = $result->getOriginalContent();
        $this->assertEquals('success', $response['status']);
        $this->assertEquals(200, $response['code']);
        $this->assertNotEmpty($response['data']);
        $this->assertInstanceOf('App\Models\Posts', $response['data']);
    }

    public function testStore_ShouldFailedValidation()
    {
        // content is required, but we don't send it
        $result = $this->json('POST', self::URL, [
            'title' => 'Unit Test',
            'published_at' => '2021-10-13T05:22:12.052Z'
        ]);

        $response = $result->getOriginalContent();
        $this->assertEquals('failed', $response['status']);
        $this->assertEquals(400, $response['code']);
        $this->assertEquals('The content field is required.', $response['message']);
    }

    public function testShow_ShouldSuccess()
    {
        $this->mockPosts();

        // content is required, but we don't send it
        $result = $this->json('GET', self::URL.'/1');
        $response = $result->getOriginalContent();
        $this->assertEquals('success', $response['status']);
        $this->assertEquals(200, $response['code']);
        $this->assertNotEmpty($response['data']);
        $this->assertInstanceOf('App\Models\Posts', $response['data']);
    }

    public function testShow_ShouldFailed()
    {
        $this->mockPosts();

        // content is required, but we don't send it
        $result = $this->json('GET', self::URL.'/100');
        $response = $result->getOriginalContent();
        $this->assertEquals('failed', $response['status']);
        $this->assertEquals(404, $response['code']);
        $this->assertEquals('Post does not exist', $response['message']);
    }

    public function testUpdate_ShouldSuccess()
    {
        $this->mockPosts();

        // content is required, but we don't send it
        $result = $this->json('PUT', self::URL.'/1', [
            'title' => 'Update unit test',
            'content' => 'Maudy Ayunda changed her post'
        ]);

        $response = $result->getOriginalContent();
        $this->assertEquals('success', $response['status']);
        $this->assertEquals(200, $response['code']);
        $this->assertNotEmpty($response['data']);
        $this->assertInstanceOf('App\Models\Posts', $response['data']);
        $this->assertEquals('Maudy Ayunda changed her post', $response['data']->content);
    }

    public function testUpdate_ShouldFailed()
    {
        $this->mockPosts();

        // content is required, but we don't send it
        $result = $this->json('PUT', self::URL.'/100', [
            'title' => 'Update unit test',
            'content' => 'Maudy Ayunda changed her post'
        ]);

        $response = $result->getOriginalContent();
        $this->assertEquals('failed', $response['status']);
        $this->assertEquals(404, $response['code']);
        $this->assertEquals('Post does not exist', $response['message']);
    }

    public function testDeleted_ShouldSuccess()
    {
        $this->mockPosts();

        // content is required, but we don't send it
        $result = $this->json('DELETE', self::URL.'/1');

        $response = $result->getOriginalContent();
        $this->assertEquals('success', $response['status']);
        $this->assertEquals(200, $response['code']);
        $this->assertEquals('Posts deleted is successfully', $response['message']);
    }

    public function testDeleted_ShouldFailed()
    {
        $this->mockPosts();

        // content is required, but we don't send it
        $result = $this->json('DELETE', self::URL.'/100');

        $response = $result->getOriginalContent();
        $this->assertEquals('failed', $response['status']);
        $this->assertEquals(404, $response['code']);
        $this->assertEquals('Post does not exist', $response['message']);
    }

    private function mockPosts()
    {
        return Posts::factory()->create([
            'title' => 'Muady Ayunda',
            'content' => 'Maudy Ayunda added a new posts'
        ]);
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }
}

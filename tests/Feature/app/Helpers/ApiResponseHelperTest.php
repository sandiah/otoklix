<?php

namespace Tests\Feature\app\Helpers;

use App\Helpers\ApiResponseHelper;
use App\Models\Posts;
use Illuminate\Foundation\Testing\WithoutEvents;
use Mockery;
use Tests\TestCase;

class ApiResponseHelperTest extends TestCase
{
    use WithoutEvents;

    protected function setUp() : void
    {
        parent::setUp();
    }

    public function testSuccess()
    {
        $posts = Posts::factory()->create([
            'title' => 'Muady Ayunda',
            'content' => 'Maudy Ayunda added a new posts'
        ]);

        $result = ApiResponseHelper::success($posts, [], 'Successfully added new post', 200);
        $response = $result->getOriginalContent();
        $this->assertEquals('success', $response['status']);
        $this->assertEquals(200, $response['code']);
        $this->assertEquals('Successfully added new post', $response['message']);
        $this->assertNotEmpty($response['data']);
    }

    public function testFailed()
    {
        $exception = new \Exception();
        $result = ApiResponseHelper::failed($exception);
        $this->assertEquals('failed', $result['status']);
        $this->assertEquals(500, $result['code']);
    }

    protected function tearDown() : void
    {
        Mockery::close();
        parent::tearDown();
    }
}

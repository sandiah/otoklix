<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Requirement
- PHP > 7.4
- Composer PHP > v2

## How to setup
- Clone project
- Run Command on your terminal/cmd (inside project)
  - `composer install`
  - `php artisan key generate`
  - `touch database/database.sqlite`
  - `touch database/dbtest.sqlite`
- Create or Edit file `.env` and `env.testing`
    - you can see an example in the `.env.example` file
    - for `.env.testing` DB_DATABASE you can set like this `DB_DATABASE=database/dbtest.sqlite`
- Run Command: `php artisan migrate and php artisan migrate --env=testing`

## How to run project
- Run command: `php artisan serve`
and u can access http://127.0.0.1:8000/api/posts

## How to run unit test
- Run command: `php artisan test`

## Contact
If you have a problem, you can contact me at email: `ahsansandiah@gmail.com`
or create ticket on menu `Issues`

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

<?php

namespace App\Services;

use App\Helpers\ApiResponseHelper;
use App\Models\Posts;

class PostsService
{
    public function add(array $params)
    {
        $posts = new Posts();
        $posts->title = $params['title'];
        $posts->content = $params['content'];
        $posts->published_at = $params['published_at'] ?? null;
        $posts->save();

        return $posts;
    }

    public function update($id, array $params)
    {
        $posts = Posts::find($id);
        if (!($posts instanceof Posts)) {
            return null;
        }

        $posts->update([
            'title' =>  $params['title'] ?? $posts->title,
            'content' => $params['content'] ?? $posts->content,
            'published_at' => $params['published_at'] ?? $posts->published_at
        ]);

        return $posts;
    }
}

<?php

namespace App\Helpers;


/**
 * Class ApiResponseHelper
 * @package App\Helpers
 */
class ApiResponseHelper
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'failed';

    public static function success($data = [], $meta = [], $message = null, $httpCode = null)
    {
        $response['status'] = self::STATUS_SUCCESS;
        $response['code'] = $httpCode ? $httpCode : 200;
        if ($message) {
            $response['message'] = $message;
        }

        if ($data) {
            $response['data'] = $data;
        }

        if ($meta) {
            $response['meta'] = $meta;
        }

        return response()->json($response);
    }

    public static function failed($exception, $httpCode = null)
    {
        if (method_exists($exception, 'getCode')) {
            $statusCode = !empty($exception->getCode()) ? $exception->getCode() : 500;
        } else {
            $statusCode = !empty($httpCode) ? $httpCode : 500;
        }

        $response = [
            'status' => self::STATUS_FAILED,
            'code' => $statusCode
        ];

        switch ($statusCode) {
            case 401:
                $messageDefault = 'Unauthorized';
                break;
            case 403:
                $messageDefault = 'Forbidden';
                break;
            case 404:
                $messageDefault = 'Not Found';
                break;
            case 405:
                $messageDefault = 'Method Not Allowed';
                break;
            case 422:
                $messageDefault = $exception->original['message'];
                $response['errors'] = $exception->original['errors'];
                break;
            default:
                $messageDefault = ($statusCode == 500) ? 'Internal server error' : $exception->getMessage();
                break;
        }

        $response['message'] = $exception->getMessage() ?? $messageDefault;

        return response()->json($response)->original;
    }

}

<?php

namespace App\Http\Controllers;

use App\Helpers\ApiResponseHelper;
use App\Models\Posts;
use App\Services\PostsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    protected  $service;

    public function __construct(PostsService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        $posts = Posts::all();

        return ApiResponseHelper::success($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'content' => 'required',
            ]);

            if ($validator->fails()) {
                throw new \Exception(implode(" ",$validator->errors()->all()), 400);
            } else {
                $posts = $this->service->add($request->all());
                $response = ApiResponseHelper::success($posts);
            }

        } catch (\Exception $e) {
            return ApiResponseHelper::failed($e);
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return
     */
    public function show($id)
    {
        try {
            $posts = Posts::find($id);
            if (!$posts instanceof Posts) {
                throw new \Exception('Post does not exist', 404);
            } else {
                $response = ApiResponseHelper::success($posts);
            }

        } catch (\Exception $e) {
            return ApiResponseHelper::failed($e);
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return
     */
    public function update(Request $request, $id)
    {
        try {
            $posts = $this->service->update($id, $request->all());
            if (is_null($posts)) {
                throw new \Exception('Post does not exist', 404);
            }

            $response = ApiResponseHelper::success($posts, [], 'Posts updated is successfully');

        } catch (\Exception $e) {
            return ApiResponseHelper::failed($e);
        }

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return
     */
    public function destroy($id)
    {
        try {
            $posts = Posts::find($id);
            if (!$posts instanceof Posts) {
                throw new \Exception('Post does not exist', 404);
            } else {
                $posts->delete();
                $response = ApiResponseHelper::success([], [],'Posts deleted is successfully');
            }

        } catch (\Exception $e) {
            return ApiResponseHelper::failed($e);
        }

        return $response;
    }
}

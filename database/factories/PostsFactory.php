<?php

namespace Database\Factories;

use App\Models\Posts;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Posts::class;

    /**
     * @inheritDoc
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text,
            'content' => $this->faker->text,
            'published_at' => $this->faker->dateTime
        ];
    }
}
